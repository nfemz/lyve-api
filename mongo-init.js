db.createUser(
    {
        user: 'admin',
        pwd: 'password',
        roles: [
            {
                role: "readWrite",
                db: "lyve_db"
            }
        ]
    }
)