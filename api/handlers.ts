import {
  APIGatewayEventRequestContext,
  APIGatewayProxyEvent
} from 'aws-lambda';
import lambdas from './lambdas';
import {
  loggerMiddleware,
  makeLambdaHandler,
  makeValidateBodyMiddleware,
  makeValidatePathParams,
  makeValidateMultiQueryStringParams,
  authorizationMiddleware
} from './middleware';
import { LyveResult } from './middleware/types';
import { Validators } from './validators';
require('dotenv').config();

/**
 * This file is the "hub" of the api.
 * Each of these handler functions receives the request, injects any desired middleware, and converts the
 * event and request-context to a generic format to work with our lambda functions.
 * They are then connected to a HTTP resource in the serverless.yml file to orchestrate everything
 */

const defaultMiddleware = [loggerMiddleware];

// Workouts
export const createWorkoutHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateBodyMiddleware(Validators.workoutInputValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.createWorkoutLambda,
    middleware,
    protectedRoute: true
  });
};

export const deleteWorkoutHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateMultiQueryStringParams(['_ids'])
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.deleteWorkoutLambda,
    middleware,
    protectedRoute: true
  });
};

export const getWorkoutHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [...defaultMiddleware];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.getWorkoutLambda,
    middleware,
    protectedRoute: true
  });
};

export const updateWorkoutHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidatePathParams(['_id']),
    makeValidateBodyMiddleware(Validators.workoutUpdateValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.updateWorkoutLambda,
    middleware,
    protectedRoute: true
  });
};

// Exercises
export const createExerciseHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateBodyMiddleware(Validators.exerciseInputValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.createExerciseLambda,
    middleware,
    protectedRoute: true
  });
};

export const deleteExerciseHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateMultiQueryStringParams(['_ids'])
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.deleteExerciseLambda,
    middleware,
    protectedRoute: true
  });
};

export const getExerciseHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [...defaultMiddleware];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.getExerciseLambda,
    middleware,
    protectedRoute: true
  });
};

export const updateExerciseHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidatePathParams(['_id']),
    makeValidateBodyMiddleware(Validators.exerciseUpdateValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.updateExerciseLambda,
    middleware,
    protectedRoute: true
  });
};

// Users
export const createUserHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateBodyMiddleware(Validators.userValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.createUserLambda,
    middleware,
    protectedRoute: true
  });
};

export const deleteUserHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidateMultiQueryStringParams(['_ids'])
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.deleteUserLambda,
    middleware,
    protectedRoute: true
  });
};

export const getUserHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [...defaultMiddleware];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.getUserLambda,
    middleware,
    protectedRoute: true
  });
};

export const updateUserHandler = async (
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext
): Promise<LyveResult> => {
  const middleware = [
    ...defaultMiddleware,
    makeValidatePathParams(['_id']),
    makeValidateBodyMiddleware(Validators.userValidator)
  ];
  return makeLambdaHandler({
    event,
    context,
    lambda: lambdas.updateUserLambda,
    middleware,
    protectedRoute: true
  });
};
