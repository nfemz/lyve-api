import Ajv, { JSONSchemaType } from 'ajv';
import { User } from '../models/resources/user.model';

const ajv = new Ajv();

export const userSchema: JSONSchemaType<User> = {
  type: 'object',
  properties: {
    email: {
      type: 'string'
    },
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    picture: {
      type: 'string'
    }
  },
  required: ['email', 'firstName', 'lastName']
};

export const userValidator = ajv.compile(userSchema);
