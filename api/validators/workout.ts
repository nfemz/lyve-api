import Ajv, { JSONSchemaType } from "ajv";
import { Workout, WorkoutExercise, WorkoutSet } from "../models/resources/workout.model";

const ajv = new Ajv();

const workoutSetSchema: JSONSchemaType<WorkoutSet> = {
    type: "object",
    properties: {
        reps: {
            type: "integer"
        },
        weight: {
            type: "number"
        },
        weightUnit: {
            type: "string"
        }
    },
    required: ["reps", "weight", "weightUnit"]
}

const workoutExerciseSchema: JSONSchemaType<WorkoutExercise> = {
    type: "object",
    properties: {
        _id: {
            type: "string"
        },
        sets: {
            type: "array",
            items: workoutSetSchema
        }
    },
    required: ["_id", "sets"]
}

const workoutSchema: JSONSchemaType<Workout> = {
    type: "object",
    properties: {
        name: {
            type: "string"
        },
        exercises: {
            type: "array",
            items: workoutExerciseSchema
        }
    },
    required: ["name", "exercises"]
};

const workoutInputSchema: JSONSchemaType<Workout[]> = {
    type: "array",
    items: workoutSchema
}

export const workoutInputValidator = ajv.compile(workoutInputSchema);
export const workoutUpdateValidator = ajv.compile(workoutSchema);