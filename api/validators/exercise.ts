import Ajv, { JSONSchemaType } from 'ajv';
import { Exercise } from '../models/resources/exercise.model';

const ajv = new Ajv();

export const exerciseSchema: JSONSchemaType<Exercise> = {
  type: 'object',
  properties: {
    name: {
      type: 'string'
    },
    bodyParts: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  required: ['name', 'bodyParts']
};

export const exerciseInputSchema: JSONSchemaType<Exercise[]> = {
  type: 'array',
  items: exerciseSchema
};

export const exerciseUpdateValidator = ajv.compile(exerciseSchema);
export const exerciseInputValidator = ajv.compile(exerciseInputSchema);
