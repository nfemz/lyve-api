import { workoutInputValidator, workoutUpdateValidator } from './workout';
import { exerciseInputValidator, exerciseUpdateValidator } from './exercise';
import { userValidator } from './user';

export type Validator =
  | typeof workoutInputValidator
  | typeof workoutUpdateValidator
  | typeof exerciseInputValidator
  | typeof exerciseUpdateValidator
  | typeof userValidator;

export const Validators = {
  workoutInputValidator,
  workoutUpdateValidator,
  exerciseInputValidator,
  exerciseUpdateValidator,
  userValidator
};
