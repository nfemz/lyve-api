import { MongoClient, Db } from 'mongodb';
require('dotenv').config();

const {
  MONGO_INITDB_ROOT_USERNAME,
  MONGO_INITDB_ROOT_PASSWORD,
  MONGO_INITDB_DATABASE,
  MONGO_CLUSTER_URL
} = process.env;

enum LyveDatabaseStatus {
  OFFLINE = 'offline',
  ONLINE = 'online'
}

export interface LyveDatabaseState {
  errors: string[] | undefined;
  status: LyveDatabaseStatus;
}

interface LyveDatabaseInfo extends LyveDatabaseState {
  user: string | undefined;
  database: string | undefined;
}

/**
 * Base level connection to MongoDB without and knowledge of individual collections.
 * Works for both local Docker DBs and deployed DBs by leveraging env vars for connection string
 */
export class LyveDatabase {
  private username: string | undefined;
  private password: string | undefined;
  private database: string | undefined;
  private clusterUrl: string | undefined;
  private status: LyveDatabaseStatus;
  private errors: string[] | undefined;
  private cachedDatabase: Db | undefined;

  constructor() {
    this.username = MONGO_INITDB_ROOT_USERNAME;
    this.password = MONGO_INITDB_ROOT_PASSWORD;
    this.database = MONGO_INITDB_DATABASE;
    this.clusterUrl = MONGO_CLUSTER_URL;
    this.status = LyveDatabaseStatus.OFFLINE;
    this.errors = undefined;
    this.cachedDatabase = undefined;
  }

  /**
   * Rather than throwing an error, adds the error message to a internal stack state
   * @param error Error string to add to error stack
   */
  private addError(error: string) {
    if (!this.errors) this.errors = [];
    this.errors.push(error);
  }

  /**
   * Runs validation on API side connection to DB and ensures all required information exists
   */
  private checkDatabaseValues(): void {
    if (!this.username) this.addError('Cannot connect to DB: Missing username');
    if (!this.password) this.addError('Cannot connect to DB: Missing password');
    if (!this.clusterUrl)
      this.addError('Cannot connect to DB: Missing clusterUrl');
    if (!this.database) this.addError('Cannot connect to DB: Missing database');
  }

  private clearErrors() {
    this.errors = undefined;
  }

  public getConnectionInfo(): LyveDatabaseInfo {
    return {
      user: this.username,
      database: this.database,
      errors: this.errors,
      status: this.status
    };
  }

  public getState(): LyveDatabaseState {
    return {
      status: this.status,
      errors: this.errors
    };
  }

  public getDatabaseClient(): Db | undefined {
    return this.cachedDatabase;
  }

  /**
   * Initializes connection to MongoDB. 
   * Clears all existing errors from cached previous connections.
   * Runs database connection validation check.
   * @returns Promise connection object to MongoDB
   */
  public async init(): Promise<LyveDatabase> {
    this.clearErrors();
    this.checkDatabaseValues();

    try {
      if (!this.cachedDatabase) {
        const client = new MongoClient(
          `mongodb://${this.username}:${this.password}@${this.clusterUrl}/${this.database}`
        );
        await client.connect();
        this.status = LyveDatabaseStatus.ONLINE;

        this.cachedDatabase = client.db(this.database);
      }
    } catch (err) {
      this.addError(`Failure connecting to DB: ${err.message}`);
    }

    return this;
  }
}
