import { Collection, InsertManyResult, UpdateResult, DeleteResult } from 'mongodb';
import { formatMongoId, formatMongoIds } from '../../utils/mongo';
import { CollectionModel } from './base.model';
import Exercise from './exercise.model';

export enum WeightUnit {
  KG = 'kg',
  LB = 'lb'
}

export interface WorkoutExercise {
  _id: string;
  sets: WorkoutSet[];
}

export interface WorkoutSet {
  reps: number;
  weight: number;
  weightUnit: WeightUnit;
}

export interface Workout {
  name: string;
  exercises: WorkoutExercise[];
}

export interface WorkoutResult extends Workout {
  _id: string;
}


class WorkoutCollection extends CollectionModel('workout')<Workout> {
  workoutCollection: Collection<Workout>

  constructor() {
    super();
    this.getDatabaseCollection().then(res => this.workoutCollection = res)
  }

  async createWorkout(workouts: Workout[]): Promise<InsertManyResult<Workout>> {
    await this.validateWorkoutExercise(workouts);
    return await this.workoutCollection.insertMany(workouts);
  }

  async deleteWorkout(_ids: string[]): Promise<DeleteResult> {
    return await this.workoutCollection.deleteMany({
      _id: {
        $in: formatMongoIds(_ids)
      }
    })
  }

  async getAllWorkouts() {
    return await this.workoutCollection.find().toArray();
  }

  async getWorkout(_ids: string[]) {
    return await this.workoutCollection.find({
      _id: {
        $in: formatMongoIds(_ids)
      }
    }).toArray();
  }

  async updateWorkout(_id: string, workout: Workout): Promise<UpdateResult> {
    await this.validateWorkoutExercise(workout);
    return await this.workoutCollection.updateOne(
      { _id: formatMongoId(_id) },
      { $set: workout }
    );
  }

  private async validateWorkoutExercise(workoutInput: Workout[] | Workout): Promise<void> {
    let exerciseIds: any[] = [];
    const invalidSetUnits = {};
    if (Array.isArray(workoutInput)) {
      workoutInput.forEach(workout => {
        workout.exercises.forEach(exercise => {
          exerciseIds.push(exercise._id);
          exercise.sets.forEach(set => {
            if (!Object.values(WeightUnit).includes(set.weightUnit)) {
              invalidSetUnits[set.weightUnit] = true;
            }
          })
        });
      });
    } else {
      workoutInput.exercises.forEach(exercise => {
        exerciseIds.push(exercise._id);
      });
    }

    const foundExercises = await Exercise.getExercise(exerciseIds);

    const foundExerciseIds = foundExercises.map(exercise => exercise._id.toString());

    const missingExerciseIds = exerciseIds.filter(exerciseId => {
      return !foundExerciseIds.includes(exerciseId);
    });

    if (missingExerciseIds.length > 0) {
      throw new Error(`Exercise _ids: ${missingExerciseIds.join(', ')} do not exist`)
    }

    if (Object.keys(invalidSetUnits).length > 0) {
      throw new Error(`Exercises contain the following invalid weight units: ${Object.keys(invalidSetUnits).join(', ')}`);
    }
  }
}

export default new WorkoutCollection();