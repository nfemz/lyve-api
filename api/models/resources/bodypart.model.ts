export enum BodyPart {
  BICEP = 'Bicep',
  TRICEP = 'Tricep',
  CHEST = 'Chest',
  BACK = 'Back',
  SHOULDERS = 'Shoulders',
  QUADS = 'Quads',
  LEGS = 'Legs',
  ABS = 'Abs'
}
