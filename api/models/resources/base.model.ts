import { Collection } from 'mongodb';
import { LyveDatabase, LyveDatabaseState } from '../../db';

/**
 * 
 * @param collectionName string value representing the name of the MongoDB collection
 * @returns a collection template to extend for individual collections
 */
export function CollectionModel(collectionName: string) {
  return class DatabaseModel<T> {
    private state: LyveDatabaseState;
    private collection: Collection<T> | undefined;

    constructor() {
      this.getDatabaseCollection().then(res => this.collection = res)
    }

    /**
     * 
     * @returns Creates a collection
     */
    private async init() {
      const database = await new LyveDatabase().init();
      const state = database.getState();
      if (!state.errors) {
        const client = database.getDatabaseClient();

        this.collection = client?.collection<T>(collectionName);
      }
      return this;
    }


    async getDatabaseCollection(): Promise<Collection<T>> {
      if (!this.collection) await this.init();
      if (!this.collection && this.state.errors) {
        throw new Error(this.state.errors?.toString());
      } else if (!this.collection) {
        throw new Error('Unexpect error trying to connect')
      }
      return this.collection;
    }
  };
}
