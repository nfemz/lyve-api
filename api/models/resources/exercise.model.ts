import {
  Collection,
  InsertManyResult,
  UpdateResult,
  DeleteResult,
  WithId
} from 'mongodb';
import { formatMongoId, formatMongoIds } from '../../utils/mongo';
import { CollectionModel } from './base.model';
import { BodyPart } from './bodypart.model';

export interface Exercise {
  name: string;
  bodyParts: BodyPart[];
}

export interface ExerciseResult extends Exercise {
  _id: string;
}

class ExerciseCollection extends CollectionModel('exercise')<Exercise> {
  exerciseCollection: Collection<Exercise>;

  constructor() {
    super();
    this.getDatabaseCollection().then((res) => (this.exerciseCollection = res));
  }

  async createExercise(
    exercises: Exercise[]
  ): Promise<InsertManyResult<Exercise>> {
    await this.validateExerciseBodyParts(exercises);
    return await this.exerciseCollection.insertMany(exercises);
  }

  async deleteExercise(_ids: string[]): Promise<DeleteResult> {
    return await this.exerciseCollection.deleteMany({
      _id: {
        $in: formatMongoIds(_ids)
      }
    });
  }

  async getAllExercises() {
    return this.exerciseCollection.find().toArray();
  }

  async getExercise(_ids: string[]) {
    return await this.exerciseCollection
      .find({
        _id: {
          $in: formatMongoIds(_ids)
        }
      })
      .toArray();
  }

  async updateExercise(_id: string, exercise: Exercise): Promise<UpdateResult> {
    await this.validateExerciseBodyParts(exercise);
    return await this.exerciseCollection.updateOne(
      { _id: formatMongoId(_id) },
      { $set: exercise }
    );
  }

  private async validateExerciseBodyParts(
    exerciseInput: Exercise[] | Exercise
  ): Promise<void> {
    const invalidBodyParts = {};

    if (Array.isArray(exerciseInput)) {
      exerciseInput.forEach((exercise) => {
        exercise.bodyParts.forEach((bodyPart) => {
          if (!Object.values(BodyPart).includes(bodyPart)) {
            invalidBodyParts[bodyPart] = true;
          }
        });
      });
    } else {
      exerciseInput.bodyParts.forEach((bodyPart) => {
        if (!Object.values(BodyPart).includes(bodyPart)) {
          invalidBodyParts[bodyPart] = true;
        }
      });
    }

    if (Object.keys(invalidBodyParts).length > 0) {
      throw new Error(
        `Exercises contain the following invalid body parts: ${Object.keys(
          invalidBodyParts
        ).join(', ')}`
      );
    }
  }
}

export default new ExerciseCollection();
