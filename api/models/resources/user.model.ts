import {
  Collection,
  UpdateResult,
  DeleteResult,
  WithId,
  InsertOneResult
} from 'mongodb';
import { formatMongoId, formatMongoIds } from '../../utils/mongo';
import { CollectionModel } from './base.model';

export interface User {
  email: string;
  firstName: string;
  lastName: string;
  picture: string;
}

export interface UserResult extends User {
  _id: string;
}

class UserCollection extends CollectionModel('user')<User> {
  userCollection: Collection<User>;

  constructor() {
    super();
    this.getDatabaseCollection().then((res) => (this.userCollection = res));
  }

  async createUser(user: User): Promise<InsertOneResult<User>> {
    return await this.userCollection.insertOne(user);
  }

  async deleteUser(_id: string): Promise<DeleteResult> {
    return await this.userCollection.deleteOne({
      _id: formatMongoId(_id)
    });
  }

  async getAllUsers() {
    return this.userCollection.find().toArray();
  }

  async getUsers(_ids: string[]) {
    return await this.userCollection
      .find({
        _id: {
          $in: formatMongoIds(_ids)
        }
      })
      .toArray();
  }

  async getUserByEmail(email: string) {
    return await this.userCollection.findOne({ email });
  }

  async updateUser(_id: string, user: User): Promise<UpdateResult> {
    return await this.userCollection.updateOne(
      { _id: formatMongoId(_id) },
      { $set: user }
    );
  }
}

export default new UserCollection();
