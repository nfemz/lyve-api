/**
 * Typesafe / failsafe way of grabbing value from a key that may or may not exist without throwing an error if it does not exist.
 * @param object Object to destructure value from
 * @param key key of value to destructure
 * @returns destructured value
 */
export function getValue(object: any, key: string) {
    try {
        return object[key];
    } catch (err) {
        return undefined;
    }
}