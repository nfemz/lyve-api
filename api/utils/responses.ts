import { ErrorObject } from "ajv";
import { LyveResult } from "../middleware/types";

export enum SuccessStatusCode {
    OK = 200,
    CREATED = 201
}

export enum ErrorStatusCode {
    BAD_REQUEST = 400,
    UNAUTHORIZED = 403,
    NOT_FOUND = 404,
    CONFLICT = 409,
    TOO_MANY = 429,
    INTERNAL_SERVER = 500
}

/**
 * Standardizes successful responses from lambdas
 * @param statusCode @enum SuccessStatusCode
 * @param body any
 * @returns standardized lambda response 
 */
export const makeLambdaResponse = (statusCode: SuccessStatusCode, body: any): LyveResult => {
    return {
        statusCode,
        body: JSON.stringify(body)
    }
}

/**
 * Standardizes error responses from lambdas
 * @param statusCode @enum ErrorStatusCode
 * @param errors any
 * @returns standardized lambda response
 */
export const makeLambdaError = (statusCode: ErrorStatusCode, errors: string[] | ErrorObject<string, Record<string, any>, unknown>[]): LyveResult => {
    return {
        statusCode,
        body: JSON.stringify({ errors })
    }
}