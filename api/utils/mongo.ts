import { ObjectId } from 'mongodb';

export const formatMongoIds = (idInput: string[]) => idInput.map(id => new ObjectId(id));


export const formatMongoId = (idInput: string) => new ObjectId(idInput);