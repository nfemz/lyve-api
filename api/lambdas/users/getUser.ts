import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Users from '../../models/resources/user.model';
import { getValue } from '../../utils/helpers';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const getUserLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { queryStringParameters } = event;
    const _ids = getValue(queryStringParameters, '_ids');
    let res;

    if (_ids) {
      res = await Users.getUsers(_ids);
    } else {
      res = await Users.getAllUsers();
    }

    return makeLambdaResponse(SuccessStatusCode.OK, res);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};
