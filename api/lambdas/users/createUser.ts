import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Users from '../../models/resources/user.model';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const createUserLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { body } = event;
    const { email } = body;

    let user = await Users.getUserByEmail(email);

    if (!user) {
      await Users.createUser(body);
      user = await Users.getUserByEmail(email);
    }

    return makeLambdaResponse(SuccessStatusCode.OK, user);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};
