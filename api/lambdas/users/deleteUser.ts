import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Users from '../../models/resources/user.model';
import { getValue } from '../../utils/helpers';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const deleteUserLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { queryStringParameters } = event;
    const _id = getValue(queryStringParameters, '_id');

    const res = await Users.deleteUser(_id);

    return makeLambdaResponse(SuccessStatusCode.OK, res);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};
