import * as workouts from './workouts';
import * as exercises from './exercises';
import * as users from './users';

export default {
  ...workouts,
  ...exercises,
  ...users
};
