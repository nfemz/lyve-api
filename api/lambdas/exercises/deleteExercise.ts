import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Exercises from '../../models/resources/exercise.model';
import { getValue } from '../../utils/helpers';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const deleteExerciseLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { queryStringParameters } = event;
    const _ids = getValue(queryStringParameters, "_ids");

    const res = await Exercises.deleteExercise(_ids);

    return makeLambdaResponse(SuccessStatusCode.OK, res);

  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};