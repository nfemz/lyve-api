import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Workouts from '../../models/resources/workout.model';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const createWorkoutLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { body } = event;
    const res = await Workouts.createWorkout(body);

    return makeLambdaResponse(SuccessStatusCode.OK, res);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};
