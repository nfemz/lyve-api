import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Workouts from '../../models/resources/workout.model';
import { getValue } from '../../utils/helpers';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const updateWorkoutLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { body, pathParameters } = event;
    const _id = getValue(pathParameters, "_id");

    const res = await Workouts.updateWorkout(_id, body);

    return makeLambdaResponse(SuccessStatusCode.OK, res);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};