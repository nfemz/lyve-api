import { LyveContext, LyveEvent, LyveResult } from '../../middleware/types';
import Workouts from '../../models/resources/workout.model';
import { getValue } from '../../utils/helpers';
import {
  ErrorStatusCode,
  makeLambdaError,
  makeLambdaResponse,
  SuccessStatusCode
} from '../../utils/responses';

export const getWorkoutLambda = async (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult> => {
  try {
    const { queryStringParameters } = event;
    const _ids = getValue(queryStringParameters, "_ids");
    let res;

    if (_ids) {
      res = await Workouts.getWorkout(_ids);
    } else {
      res = await Workouts.getAllWorkouts();
    }

    return makeLambdaResponse(SuccessStatusCode.OK, res);
  } catch (err) {
    return makeLambdaError(ErrorStatusCode.BAD_REQUEST, err.message);
  }
};