import { APIGatewayEventRequestContext, APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";

export type LyveMiddlewareFunction = (event: LyveEvent, context: LyveContext) => [LyveEvent, LyveContext];

export type LyveLambda = (event: LyveEvent, context: LyveContext) => Promise<LyveResult>;

export interface LyveEvent extends Omit<APIGatewayProxyEvent, "body"> {
    body: any | any[];
    middlewareErrors: any[];
    queryStringParameters: {
        [name: string]: string;
    }
    multiValueQueryStringParameters: {
        [name: string]: string[];
    }
    authorizationToken: string;
    methodArn: any;
}

export interface LyveContext extends APIGatewayEventRequestContext { }

export interface LyveResult extends APIGatewayProxyResult { }