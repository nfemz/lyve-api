import {
  APIGatewayEventRequestContext,
  APIGatewayProxyEvent
} from 'aws-lambda';
import { ErrorStatusCode, makeLambdaError } from '../utils/responses';
import { authorizationMiddleware } from './authorization';
import {
  LyveContext,
  LyveEvent,
  LyveLambda,
  LyveMiddlewareFunction,
  LyveResult
} from './types';

/**
 * This function helps D.R.Y out lambda code generation by running any common data manipulation
 * that would normally run in a lambda function before actually hitting the lambda function.
 * For example, request body validation, logging, etc.
 * If any errors occur in middleware they are pushed to an error stack and the request execution is halted
 * before hiting the lambda
 * @param event the request event coming from a user
 * @param context the request context coming from a user
 * @param lambda the lambda function to ultimately create
 * @param middleware any middleware to run
 */
export const makeLambdaHandler = async ({
  event,
  context,
  lambda,
  middleware,
  protectedRoute = false
}: {
  event: APIGatewayProxyEvent;
  context: APIGatewayEventRequestContext;
  lambda: LyveLambda;
  middleware?: LyveMiddlewareFunction[];
  protectedRoute?: boolean;
}): Promise<LyveResult> => {
  let lyveEvent = event as unknown as LyveEvent;
  let lyveContext = context as unknown as LyveContext;
  let res;

  if (protectedRoute) {
    const authorizationFailure = await authorizationMiddleware(
      lyveEvent,
      lyveContext
    );

    if (authorizationFailure) return authorizationFailure;
  }

  let body = event.body;
  if (body) lyveEvent.body = JSON.parse(body);

  lyveEvent.middlewareErrors = [];

  const { queryStringParameters } = lyveEvent;

  if (queryStringParameters) {
    const newQueryStringParameters = {};
    Object.entries(queryStringParameters).forEach(([key, value]) => {
      newQueryStringParameters[key] = value.split(',');
    });
    lyveEvent.queryStringParameters = newQueryStringParameters;
  }

  middleware &&
    middleware.forEach((middlewareFunction) => {
      [lyveEvent, lyveContext] = middlewareFunction(lyveEvent, lyveContext);
    });

  const { middlewareErrors } = lyveEvent;

  if (middlewareErrors.length > 0) {
    res = makeLambdaError(ErrorStatusCode.BAD_REQUEST, middlewareErrors);
  } else {
    res = await lambda(lyveEvent, context);
  }

  console.debug('RESPONSE', res);

  return res;
};

export { loggerMiddleware } from './logger';
export { makeValidateBodyMiddleware } from './validateBody';
export { makeValidatePathParams } from './validatePathParams';
export { makeValidateQueryStringParams } from './validateQueryParams';
export { makeValidateMultiQueryStringParams } from './validateMultiQueryParams';
export { authorizationMiddleware } from './authorization';
