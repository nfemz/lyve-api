import { LyveContext, LyveEvent, LyveResult } from './types';
import * as jwt from 'jsonwebtoken';
import * as jwksClient from 'jwks-rsa';
import { ErrorStatusCode, makeLambdaError } from '../utils/responses';
const { AUTH0_CLIENT_ID, AUTH0_TOKEN_ISSUER, JWKS_URI } = process.env;

export const authorizationMiddleware = (
  event: LyveEvent,
  context: LyveContext
): Promise<LyveResult | void> => {
  const { headers } = event;
  const { Authorization } = headers;

  if (!Authorization || !JWKS_URI) {
    return new Promise((resolve, reject) => {
      resolve(makeLambdaError(ErrorStatusCode.UNAUTHORIZED, ['Unauthorized']));
    });
  }
  const [tokenType, token] = Authorization.split(' ');

  if (!(tokenType.toLowerCase() === 'bearer' && token)) {
    return new Promise((resolve, reject) => {
      resolve(makeLambdaError(ErrorStatusCode.UNAUTHORIZED, ['Unauthorized']));
    });
  }

  const client = jwksClient({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 10,
    jwksUri: JWKS_URI
  });

  const options = {
    audience: AUTH0_CLIENT_ID,
    issuer: AUTH0_TOKEN_ISSUER
  };

  const decoded = jwt.decode(token, { complete: true });

  if (!decoded || !decoded.header || !decoded.header.kid) {
    return new Promise((resolve, reject) => {
      resolve(makeLambdaError(ErrorStatusCode.UNAUTHORIZED, ['Unauthorized']));
    });
  }

  return client
    .getSigningKey(decoded.header.kid)
    .then((key) => {
      if (key) {
        const signingKey = key.getPublicKey();
        jwt.verify(token, signingKey as any, options);
      }
    })
    .catch((err) =>
      makeLambdaError(ErrorStatusCode.UNAUTHORIZED, ['Unauthorized'])
    );
};
