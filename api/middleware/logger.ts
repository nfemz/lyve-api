import { LyveContext, LyveEvent } from './types';

/**
 * Passthrough middleware to log request information
 * @param event AWS Lambda Event extended to own application Event
 * @param context AWS Lambda Context extended to own application Context
 */
export const loggerMiddleware = (
  event: LyveEvent,
  context: LyveContext
): [LyveEvent, LyveContext] => {
  const {
    pathParameters,
    queryStringParameters,
    body,
    headers: { Authorization }
  } = event;

  console.debug('REQUEST', {
    pathParameters,
    queryStringParameters,
    body,
    Authorization
  });

  return [event, context];
};
