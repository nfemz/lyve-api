import { Validator } from '../validators';
import { LyveContext, LyveEvent } from './types';

/**
 * Passthrough middleware to ensure body sent in request exists and matches provided schema
 * @param event AWS Lambda Event extended to own application Event
 * @param context AWS Lambda Context extended to own application Context
 */
export const makeValidateBodyMiddleware = (validator?: Validator) => {
  return (
    event: LyveEvent,
    context: LyveContext
  ): [LyveEvent, LyveContext] => {
    const {
      body,
    } = event;

    if (!body) event.middlewareErrors.push('Missing request body');

    if (validator && !validator(body)) {
      validator.errors ?
        event.middlewareErrors = event.middlewareErrors.concat(validator.errors) :
        event.middlewareErrors.push('Invalid request body and validator failed to parse');
    }

    return [event, context];
  };
}
