import { LyveContext, LyveEvent } from "./types"

/**
 * Passthrough middleware to ensure pathparams matches provided params
 * @param event AWS Lambda Event extended to own application Event
 * @param context AWS Lambda Context extended to own application Context
 */
export const makeValidatePathParams = (params: string[]) => {
    return (event: LyveEvent, context: LyveContext): [LyveEvent, LyveContext] => {
        const { pathParameters } = event;
        const missingPathParams: string[] = [];

        if (pathParameters) {
            params.forEach(param => {
                if (!pathParameters[param]) missingPathParams.push(param)
            })
        } else {
            event.middlewareErrors.push(`Missing Path Parameters: ${params.join(', ')}`);
        }

        if (missingPathParams.length) {
            event.middlewareErrors.push(`Missing Path Parameters: ${missingPathParams.join(', ')}`);
        }

        return [event, context]
    }
}