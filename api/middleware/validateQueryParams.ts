import { LyveContext, LyveEvent } from "./types"

/**
 * Passthrough middleware to ensure querystringparams matches provided params
 * @param event AWS Lambda Event extended to own application Event
 * @param context AWS Lambda Context extended to own application Context
 */
export const makeValidateQueryStringParams = (params: string[]) => {
    return (event: LyveEvent, context: LyveContext): [LyveEvent, LyveContext] => {
        const { queryStringParameters } = event;
        const missingQueryStringParams: string[] = [];

        if (queryStringParameters) {
            params.forEach(param => {
                if (!queryStringParameters[param]) missingQueryStringParams.push(param)
            })
        } else {
            event.middlewareErrors.push(`Missing Query String Parameters: ${params.join(', ')}`);
        }

        if (missingQueryStringParams.length) {
            event.middlewareErrors.push(`Missing Query String Parameters: ${missingQueryStringParams.join(', ')}`);
        }

        return [event, context]
    }
}